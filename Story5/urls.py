from django.urls import path
from . import views

app_name = 'Story5'

urlpatterns = [
    path('', views.courses, name="courses"),
    path('add/', views.addCourse, name="form"),
    path('description/<int:id>', views.descCourse, name="description"),
    path('delete/<int:id>', views.deleteCourse, name="delete"),
]