from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import *
from .forms import CourseForm


def courses(request):
	course = Course.objects.all()
	context = {
		"type" : "course",
		"courses" : course
	}
	return render(request, 'home/courses.html', context)


def addCourse(request):
	form = CourseForm()
	if request.method == 'POST':
		form = CourseForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('Story5:courses')
	context = {
		'form' : form
	}
	return render(request, 'home/form.html', context)


def descCourse(request, id):
	desc = Course.objects.get(id=id)
	context = {
		'desc' : desc
	}
	return render(request, 'home/courseDesc.html', context)


def deleteCourse(request, id):
	if request.method == 'POST':
		course = Course.objects.get(id=id)
		course.delete()
		
	return redirect('Story5:courses')