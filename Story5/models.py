from django.db import models
from django.utils import timezone
from datetime import datetime, date

class Course(models.Model):
	courseName = models.CharField(max_length=100)
	lecturer = models.CharField(max_length=50)
	credit = models.CharField(max_length=5)
	semester = models.CharField(max_length=5)

	def __str__(self):
		return self.courseName
