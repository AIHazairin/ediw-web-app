from django.urls import path

from . import views

app_name = 'Story'

urlpatterns = [
    path('', views.index, name='index'),
]