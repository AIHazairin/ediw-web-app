from django.shortcuts import render

# Create your views here.

def index(request):
	context = {
		"type" : "Story"
	}
	return render(request, 'home/index.html', context)