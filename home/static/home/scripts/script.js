// to make page back to top when reloading
$(window).on('beforeunload', function() {
  $('body').hide();
  $(window).scrollTop(0);
});

// parallax on greetings
$(window).on('load', function() {
	$("#greetings").addClass('parallax-items');
	$("#easteregg").addClass('parallax-items');
	$("#frontphoto").addClass('parallax-items');
	$(".photo-shadow").addClass('parallax-items');
});