from django.shortcuts import render

# Create your views here.

def index(request):
	context = {
		"type" : "homepage"
	}
	return render(request, 'home/homepage.html', context)

def facts_page(request):
	context = {
		"type" : "facts"
	}
	return render(request, 'home/facts-page.html', context)