from django.urls import path
from . import views

app_name = 'Story6'

urlpatterns = [
    path('', views.view, name="activities"),
    path('addPerson/<int:id>', views.addPerson, name="addPerson"),
    path('addActivity/', views.addActivity, name="addActivity"),
    path('delPerson/<int:id>', views.delPerson, name="deletePerson"),
    path('delActivity/<int:id>', views.delActivity, name="deleteActivity")
]