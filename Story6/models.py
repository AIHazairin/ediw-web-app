from django.db import models

class Activities(models.Model):
	name = models.CharField(max_length=50)

	def __str__(self):
		return self.name

class Person(models.Model):
	name = models.CharField(max_length=50)
	activity = models.ManyToManyField(Activities)

	def __str__(self):
		return self.name