from django.test import TestCase, Client
from django.urls import resolve, reverse
from .models import Activities, Person
from .views import view, addPerson, addActivity, delPerson, delActivity
# Create your tests here.


class TestModel(TestCase):
	"""docstring for TestStory6"""
	def setUp(self):
		self.person_dummy = Person.objects.create(
			name = "participant"
			)
		self.activity_dummy = Activities.objects.create(
			name = "kegiatan"
			)
		self.person_dummy.activity.add(self.activity_dummy)
	
	def test_name(self):
		self.assertEquals(str(self.person_dummy), "participant")

	def test_act_name(self):
		self.assertEquals(str(self.activity_dummy), "kegiatan")

class TestViews(TestCase):
	def setUp(self):
		self.client = Client()
		self.view_url = reverse("Story6:activities")
		self.activity_dummy = Activities.objects.create(name="kegiatan")
		self.person_dummy = Person.objects.create(name="participant")
		self.add_person = reverse("Story6:addPerson", args=[self.activity_dummy.id])
		self.add_activity = reverse("Story6:addActivity")

	def test_view_GET(self):
		response = self.client.get(self.view_url)
		self.assertEquals(response.status_code, 200)
		self.assertTemplateUsed(response, "home/activities.html")
		self.assertContains(response, "Activities")
