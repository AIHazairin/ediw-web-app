from django.forms import ModelForm
from .models import Activities, Person

class ActivityForm(ModelForm):
  class Meta:
    model = Activities
    fields = '__all__'
  error_messages = {
    'required' : 'Please fill this field'
  }

class PersonForm(ModelForm):
  class Meta:
    model = Person
    fields = '__all__'
  error_messages = {
    'required' : 'Please fill this field'
  }