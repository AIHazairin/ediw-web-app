from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import *
from .forms import ActivityForm, PersonForm
# Create your views here.

def view(request):
	person = Person.objects.all()
	activity = Activities.objects.all()
	person_form = PersonForm()
	activity_form = ActivityForm()
	context = {
		"type" : "activities",
		"person" : person,
		"activities" : activity,
		"person_form": person_form,
		"activity_form": activity_form,
	}
	print(person)
	return render(request, 'home/activities.html', context)

def addPerson(request, id):
	form = PersonForm()
	act = Activities.objects.get(id=id)

	if request.method == 'POST':
		form = PersonForm(request.POST)
		person = Person.objects.create(name=request.POST["name"])
		if form.is_valid():
			person.save()
			person.activity.add(act)
			return redirect('Story6:activities')
	return redirect('Story6:activities')

def addActivity(request):
	form = ActivityForm()
	if request.method ==  'POST':
		form = ActivityForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('Story6:activities')

def delPerson(request, id):
	if request.method == 'POST':
		person = Person.objects.get(id=id)
		person.delete()
	return redirect('Story6:activities')

def delActivity(request, id):
	if request.method == 'POST':
		activity = Activities.objects.get(id=id)
		activity.delete()
	return redirect('Story6:activities')
